module.exports = {
    mode: 'jit',
    purge: {
        content: [
            './src/app/**/*.{html,ts}',
        ]
    },
    theme: {
        fontFamily: {
            'starjedi': 'Star Jedi',
            'poppins': 'Poppins',
            'distant-galaxy': 'Distant Galaxy'
        },
        colors: {
            'black': '#000',
            'dark-gray': '#333',
            'gray': '#8a8a8a',
            'white': '#fff',
            'orange': ' #F7B500',
            'red': '#FF3232',
            'blue': '#00D2FF',
            'green': '#2ECC71'
        },
        screens: {
            'sm': '600px',            
            'md': '768px',
            'lg': '992px',
            'xl': '1280px',
            '2xl': '1536px',
        },
        backgroundColor: (theme) => theme('colors'),
        extend: {
            fontSize: {
                '4xl': '40px',
                '7xl': '70px',
                '9xl': '90px',
            },
            borderWidth: {
                '6': '6px'
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('tailwindcss-debug-screens')
    ],
};
