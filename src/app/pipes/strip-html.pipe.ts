import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'strip' })
export class StripHtmlPipe implements PipeTransform {
  transform(value: string | undefined): any {
    return !value ? '' : value.replace(/<.*?>/g, ' ')
  }
}

//kite.js.org | https://stackoverflow.com/a/46271749
