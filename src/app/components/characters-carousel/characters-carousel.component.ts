import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core'
import { Observable, of } from 'rxjs'
import { Character } from 'src/types/character'
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper'
import { SwiperComponent } from 'swiper/angular'

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y])

@Component({
  selector: 'app-characters-carousel',
  templateUrl: './characters-carousel.component.html',
})
export class CharactersCarouselComponent implements OnInit {
  @Input() characters$: Observable<Character[]> = of([])
  @Input() activeIndex = 0
  @Output() activeIndexChange: EventEmitter<number> = new EventEmitter<number>()
  @ViewChild(SwiperComponent) swiper?: SwiperComponent

  characters: any[] = []
  pagination = {
    el: '.swiper-pagination-wrapper',
    clickable: true,
    renderBullet: (i: number, className: string) => {
      if (this.characters.length > 0) {
        className += ` ${
          this.characters[i].selected ? 'selected-character' : ''
        }`
      }
      return `<span class="${className}"></span>`
    },
  }

  ngOnInit(): void {
    this.characters$.subscribe((characters) => {
      this.characters = characters
      this.updateSwiperPagination()
    })
  }

  onSlideChange() {
    this.activeIndex = this.swiper?.swiperRef.activeIndex as number
    this.activeIndexChange.emit(this.activeIndex)
    this.updateSwiperPagination()
  }

  private updateSwiperPagination() {
    this.swiper?.swiperRef.pagination.render()
    this.swiper?.swiperRef.pagination.update()
  }

  next() {
    this.swiper?.swiperRef.slideNext()
  }

  prev() {
    this.swiper?.swiperRef.slidePrev()
  }
}
