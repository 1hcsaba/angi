import { trigger, style, transition, animate } from '@angular/animations'
import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Observable } from 'rxjs'
import { delay } from 'rxjs/operators'
import { Character } from 'src/types/character'

@Component({
  selector: 'app-simulation-panel',
  templateUrl: './simulation-panel.component.html',
  styleUrls: ['./simulation-panel.style.scss'],
  animations: [
    trigger('fadeOut', [
      transition(':leave', [
        animate(
          500,
          style({
            opacity: 0,
            height: 0,
          }),
        ),
      ]),
    ]),
  ],
})
export class SimulationPanelComponent implements OnInit {
  @Input() character$!: Observable<Character>
  @Input() side!: string
  @Input() active = ''

  state = false

  constructor(private router: Router) {}

  goToHome() {
    this.router.navigate(['/'])
  }

  ngOnInit(): void {
    this.character$.pipe(delay(0)).subscribe((character) => {
      this.state = character.won as boolean
    })
  }
}
