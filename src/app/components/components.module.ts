import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { IconComponent } from './icon/icon.component'
import { CharactersCarouselComponent } from './characters-carousel/characters-carousel.component'
import { SwiperModule } from 'swiper/angular'
import { ProgressComponent } from './progress/progress.component'
import { SimulationPanelComponent } from './simulation-panel/simulation-panel.component'
import { StripHtmlPipe } from '../pipes/strip-html.pipe'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ImageComponent } from './image/image.component'

@NgModule({
  declarations: [
    IconComponent,
    CharactersCarouselComponent,
    ProgressComponent,
    SimulationPanelComponent,
    StripHtmlPipe,
    ImageComponent,
  ],
  exports: [
    IconComponent,
    CharactersCarouselComponent,
    ProgressComponent,
    SimulationPanelComponent,
    ImageComponent,
  ],
  imports: [CommonModule, SwiperModule, BrowserAnimationsModule],
})
export class ComponentsModule {}
