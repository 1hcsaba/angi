import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
})
export class IconComponent {
  @Input() name!: string
  @Input() width: string | number = 16
  @Input() height: string | number = 16
}
