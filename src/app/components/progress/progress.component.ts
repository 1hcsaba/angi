import { Component, HostBinding, Input } from '@angular/core'

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
})
export class ProgressComponent {
  @HostBinding('style.--value')
  @Input()
  value = 0

  @HostBinding('style.--min')
  @Input()
  min = 0

  @HostBinding('style.--max')
  @Input()
  max = 0

  @HostBinding('style.--color')
  @Input()
  color = 'red'

  @Input() mirrored = false
}
