import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SwiperModule } from 'swiper/angular'
import { LobbyComponent } from './lobby.component'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  declarations: [LobbyComponent],
  imports: [CommonModule, SwiperModule, ComponentsModule],
})
export class LobbyModule {}
