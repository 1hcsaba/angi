import { Component, NgZone, OnInit } from '@angular/core'
import { first, map, switchMap } from 'rxjs/operators'
import { CharacterService } from '../../services/character.service'

import { Router } from '@angular/router'
import { BehaviorSubject, Observable, of } from 'rxjs'
import { Character } from 'src/types/character'
import { FightParticipants } from 'src/types/simulation'

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.style.scss'],
})
export class LobbyComponent implements OnInit {
  charactersSubject: BehaviorSubject<Character[]> = new BehaviorSubject<
    Character[]
  >([])
  characters: Observable<Character[]> = this.charactersSubject.asObservable()

  participants!: FightParticipants

  activeIndex = 0
  sameSideWarning = false

  constructor(
    private charachter: CharacterService,
    private router: Router,
    private ngZone: NgZone,
  ) {}

  ngOnInit(): void {
    this.charachter
      .findAll()
      .pipe(first())
      .subscribe((characters) => {
        characters[0].selected = true
        characters[characters.length - 1].selected = true

        this.charactersSubject.next(characters)
      })
  }

  get current(): Observable<Character> {
    return this.characters.pipe(
      switchMap((characters: Character[]) => of(characters[this.activeIndex])),
    )
  }

  get readyToFight(): Observable<boolean> {
    return this.selected.pipe(
      switchMap((selected: Character[]) => of(selected.length === 2)),
    )
  }

  onChange(index: number) {
    this.ngZone.run(() => {
      this.activeIndex = index
      this.sameSideWarning = false
    })
  }

  get selected(): Observable<Character[]> {
    return this.characters.pipe(
      map((characters: Character[]) =>
        characters.filter((character: Character) => character.selected),
      ),
    )
  }

  fight(): void {
    this.selected.pipe(first()).subscribe((participants) => {
      this.router.navigate(['/simulation'], {
        state: {
          participants,
        },
      })
    })
  }

  selectCharacter(): void {
    this.characters
      .pipe(
        first(),
        map((characters) => {
          const character = characters[this.activeIndex]
          const selected = characters.filter(
            (c) => c.side === character.side && c.selected === true,
          )
          let updateDue = false

          if (character.selected) {
            character.selected = !character.selected
            updateDue = true
          } else {
            if (selected.length === 0) {
              character.selected = true
              updateDue = true
            } else {
              this.sameSideWarning = true
            }
          }

          if (updateDue) {
            this.charactersSubject.next(characters)
          }
        }),
      )
      .subscribe()
  }
}
