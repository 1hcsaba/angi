import { Component } from '@angular/core'
import { FormBuilder, FormControl } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from '../../services/auth.service'
import { trigger, style, animate, transition } from '@angular/animations'
import { catchError } from 'rxjs/operators'
import { of } from 'rxjs'
import { Nullable } from 'src/types'
import { UserCredential } from '../../../types/auth'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  animations: [
    trigger('appear', [
      transition(':enter', [
        style({
          'max-height': 0,
          opacity: 0,
        }),
        animate(
          '200ms',
          style({
            'max-height': 48,
            opacity: 1,
          }),
        ),
      ]),
      transition(':leave', [
        style({
          'max-height': 48,
          opacity: 1,
        }),
        animate(
          '100ms',
          style({
            'max-height': 0,
            opacity: 0,
          }),
        ),
      ]),
    ]),
  ],
})
export class LoginComponent {
  loginForm = this.formBuilder.group({
    username: new FormControl('frontend@webstar.hu'),
    password: new FormControl(''),
  })
  errorOccurred = false

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
  ) {
    if (this.authService.userValue) {
      this.router.navigate(['/'])
    }
  }

  onSubmit() {
    this.errorOccurred = false
    const { username, password } = this.loginForm.value
    this.loginForm.valid
    this.authService
      .login(username, password)
      .pipe(catchError(() => of(null)))
      .subscribe((user: Nullable<UserCredential>) => {
        if (!user) {
          this.errorOccurred = true
        } else {
          this.router.navigate(['/'])
        }
      })
  }
}
