import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SimulationComponent } from './simulation.component'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  declarations: [SimulationComponent],
  imports: [CommonModule, ComponentsModule],
})
export class SimulationModule {}
