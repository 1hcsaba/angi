import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { delay, filter, first, map, takeWhile } from 'rxjs/operators'
import { CharacterService } from '../../services/character.service'
import { BehaviorSubject, Observable, timer, zip } from 'rxjs'
import { Character } from 'src/types/character'
import { environment } from 'src/environments/environment'

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
})
export class SimulationComponent implements OnInit {
  seed = 0
  winnerSide = ''

  participantsSubject: {
    light: BehaviorSubject<Character>
    dark: BehaviorSubject<Character>
  }
  lightCharacter$: Observable<Character>
  darkCharacter$: Observable<Character>

  constructor(private router: Router, private character: CharacterService) {
    const participants: Character[] =
      this.router.getCurrentNavigation()?.extras?.state?.participants

    if (!participants) {
      this.router.navigate(['/'])
    }

    this.participantsSubject = {
      dark: new BehaviorSubject(this.find(participants, 'DARK')),
      light: new BehaviorSubject(this.find(participants, 'LIGHT')),
    }

    this.lightCharacter$ = this.participantsSubject.light.asObservable()
    this.darkCharacter$ = this.participantsSubject.dark.asObservable()
  }

  private find(characters: Character[], side: string): Character {
    return characters.find(
      (character: Character) => character.side === side,
    ) as Character
  }

  get hasWinner() {
    return zip(this.darkCharacter$, this.lightCharacter$).pipe(
      first(),
      map((characters) => characters.some((x) => x.won === true)),
    )
  }

  private getRandomValue() {
    //https://stackoverflow.com/a/19303725
    const x = Math.sin(this.seed++) * 10000

    return Math.round(x - Math.floor(x))
  }

  private opposite(side: string) {
    return side === 'light' ? 'dark' : 'light'
  }

  async ngOnInit(): Promise<void> {
    this.character
      .simulate(zip(this.darkCharacter$, this.lightCharacter$))
      .pipe(first())
      .subscribe(async ({ simulationId }: any) => {
        this.seed = Number(atob(simulationId))

        timer(2000, 1000)
          .pipe(
            map(() => this.getRandomValue()),
            map((i: number) =>
              Object.values(this.participantsSubject)[i].getValue(),
            ),
            map((character) => {
              character.health -= Math.abs(environment.simulation.damage)
              character.health = character.health < 0 ? 0 : character.health

              return character
            }),
            takeWhile((character: Character) => character.health > 0, true),
            filter((character) => character.health <= 0),
            delay(500),
          )
          .subscribe((character) => {
            const side: 'dark' | 'light' = character.side.toLowerCase() as
              | 'dark'
              | 'light'
            const winnerSide = this.opposite(side)
            const subject = this.participantsSubject[winnerSide]

            const characterValue = subject.getValue()
            characterValue.won = true

            this.winnerSide = winnerSide
            subject.next(characterValue)
          })
      })
  }
}
