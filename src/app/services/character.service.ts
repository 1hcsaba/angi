import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { first, map, switchMap } from 'rxjs/operators'
import { environment } from 'src/environments/environment'
import { Character, CharacterResponse } from 'src/types/character'
import { FightParticipants } from 'src/types/simulation'

@Injectable({
  providedIn: 'root',
})
export class CharacterService {
  constructor(private http: HttpClient) {}

  findAll(): Observable<Character[]> {
    return this.http
      .get<CharacterResponse>(`${environment.apiURL}/characters/`)
      .pipe(
        first(),
        map((result: CharacterResponse) => {
          for (const character of result.characters) {
            Object.assign(character, {
              selected: false,
              dark: character.side === 'DARK',
              won: false,
              health: 100,
            })
          }
          return result.characters as Character[]
        }),
      )
  }

  private format(
    participants: Observable<Character[]>,
  ): Observable<FightParticipants> {
    return participants.pipe(
      first(),
      map((participants: Character[]) => {
        return participants.reduce((acc, curr: Character) => {
          return Object.assign(acc, {
            [curr.side.toLowerCase()]: curr.id,
          })
        }, {}) as FightParticipants
      }),
    )
  }

  simulate(participants: Observable<Character[]>) {
    return this.format(participants).pipe(
      first(),
      switchMap((data: any) =>
        this.http.post(`${environment.apiURL}/simulate/`, data),
      ),
    )
  }
}
