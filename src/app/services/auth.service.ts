import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { mergeMap } from 'rxjs/operators'
import { User, UserCredential } from '../../types/auth'
import { StorageService } from './storage.service'
import { Router } from '@angular/router'
import { BehaviorSubject, Observable } from 'rxjs'
import { Nullable } from 'src/types'

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  apiURL = environment.apiURL

  private userSubject: BehaviorSubject<Nullable<User>>
  public user: Observable<Nullable<User>>

  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private router: Router,
  ) {
    this.userSubject = new BehaviorSubject<Nullable<User>>(
      this.storage.getUser(),
    )
    this.user = this.userSubject.asObservable()
  }

  /*
  private mockLogin(): Observable<UserCredential> {
    return new BehaviorSubject<UserCredential>({
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhc2Rhc2Rhc2Rhc2Rhc2Rhc2QifQ.2cOfI5PKBkkYI8rFHyPhxgn9oPEXfoVPwO16_xfPQ6Y",
      "refreshToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJyZWZyZXNoVG8zMjRTQURrZW5BU0QifQ.TWRdrBPMHiSHPRPn9-eGILboste11I9wgYg44eWKwd4",
      "user": {
        "email": "frontend@webstar.hu",
        "firstName": "Pilóta",
        "lastName": "Felvételiző"
      }
    })
  }
  */

  private signInWithEmailAndPassword(
    username: string,
    password: string,
  ): Observable<UserCredential> {
    return this.http.post<UserCredential>(
      environment.apiURL + '/authentication/',
      {
        username,
        password,
      },
    )
  }

  login(username: string, password: string) {
    /*return this.mockLogin().pipe(
        mergeMap(async (credential: UserCredential) => {
          await this.saveAccessToken(credential)

          return credential
        })
      )*/
    return this.signInWithEmailAndPassword(username, password).pipe(
      mergeMap(async (credential: UserCredential) => {
        await this.saveAccessToken(credential)

        return credential
      }),
    )
  }

  public get userValue(): Nullable<User> {
    return this.userSubject.value
  }

  async logout() {
    await this.storage.clear()
    this.userSubject.next(null)
    this.router.navigate(['/login'])
  }

  async saveAccessToken(credential: UserCredential): Promise<void> {
    await this.storage.saveCredentials(credential)
    this.userSubject.next(credential.user)
  }
}
