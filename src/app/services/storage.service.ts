import { Injectable } from '@angular/core'
import * as localforage from 'localforage'
import { Nullable } from 'src/types'
import { User, UserCredential } from '../../types/auth'

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  user: User | null
  constructor() {
    this.user = null
  }

  async saveCredentials(credential: UserCredential) {
    await localforage.setItem('user', credential.user)
    await localforage.setItem('accessToken', credential.token)
    await localforage.setItem('refreshToken', credential.refreshToken)
  }

  setUserInMemory(user: User | null) {
    this.user = user
  }

  getUser(): User | null {
    return this.user
  }

  loadUser() {
    return localforage.getItem<Nullable<User>>('user')
  }

  getAccessToken(): Promise<string | null> {
    return localforage.getItem('accessToken')
  }

  clear() {
    return Promise.all([
      localforage.removeItem('user'),
      localforage.removeItem('accessToken'),
      localforage.removeItem('refreshToken'),
    ])
  }
}
