import { APP_INITIALIZER, NgModule } from '@angular/core'
import { StorageService } from './services/storage.service'

const initalizeApp = (storage: StorageService) => {
  return async () => {
    const user = await storage.loadUser()
    storage.setUserInMemory(user)
  }
}

@NgModule({
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initalizeApp,
      deps: [StorageService],
      multi: true,
    },
  ],
})
export class HydrateModule {}
