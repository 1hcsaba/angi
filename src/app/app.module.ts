import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing.module'
import { httpInterceptorProviders } from './app.interceptor'
import { HydrateModule } from './hydrate.module'
import { NavigationComponent } from './components/navigation/navigation.component'
import { SymbolsComponent } from './components/symbols/symbols.component'
import { ComponentsModule } from './components/components.module'
import { LoginModule } from './routes/login/login.module'
import { SimulationModule } from './routes/simulation/simulation.module'
import { LobbyModule } from './routes/lobby/lobby.module'

@NgModule({
  declarations: [AppComponent, NavigationComponent, SymbolsComponent],
  imports: [
    HydrateModule,
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    LobbyModule,
    SimulationModule,
    LoginModule,
  ],
  exports: [ComponentsModule],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}
