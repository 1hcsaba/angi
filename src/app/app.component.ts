import {
  Component,
  HostBinding,
  OnInit,
  ViewEncapsulation,
} from '@angular/core'
import { Router, RoutesRecognized } from '@angular/router'
import { filter } from 'rxjs/operators'
import { Nullable } from 'src/types'
import { User } from '../types/auth'
import { AuthService } from './services/auth.service'

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  user: Nullable<User>

  @HostBinding('style.--bg')
  bg!: string
  @HostBinding('style.--bg2x')
  bg2x!: string

  constructor(private authService: AuthService, private router: Router) {
    this.user = this.authService.userValue
  }

  async ngOnInit() {
    this.authService.user.subscribe((user: Nullable<User>) => {
      this.user = user
    })

    this.router.events
      .pipe(filter((event) => event instanceof RoutesRecognized))
      .subscribe((event: Partial<RoutesRecognized>) => {
        const { background } = event?.state?.root.firstChild?.data as {
          background?: string
        }

        if (background) {
          this.bg = `url(assets/${background}.jpg)`
          this.bg2x = `url(assets/${background}@2x.jpg)`
        }
      })
  }

  async logout() {
    await this.authService.logout()
  }

  get loggedIn() {
    return !!this.user
  }
}
