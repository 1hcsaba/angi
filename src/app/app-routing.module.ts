import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { LoggedInGuard } from './guards/auth.guard'
import { LoginComponent } from './routes/login/login.component'
import { SimulationComponent } from './routes/simulation/simulation.component'
import { LobbyComponent } from './routes/lobby/lobby.component'

const routes: Routes = [
  {
    path: '',
    component: LobbyComponent,
    canActivate: [LoggedInGuard],
    data: { background: 'bg_2' },
  },
  {
    path: 'simulation',
    component: SimulationComponent,
    canActivate: [LoggedInGuard],
    data: { background: 'bg_3' },
  },
  { path: 'login', component: LoginComponent, data: { background: 'bg_1' } },
  { path: '**', redirectTo: '' },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
