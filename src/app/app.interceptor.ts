import { Injectable } from '@angular/core'
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS,
} from '@angular/common/http'
import { from, Observable } from 'rxjs'
import { environment } from 'src/environments/environment'
import { StorageService } from './services/storage.service'

@Injectable()
export class AppInterceptor implements HttpInterceptor {
  constructor(private storage: StorageService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    return from(this.handleAccess(request, next))
  }

  async handleAccess(request: HttpRequest<unknown>, next: HttpHandler) {
    const accessToken = await this.storage.getAccessToken()

    const req = request.clone({
      headers: request.headers
        .set('Applicant-Id', environment.applicantId)
        .set('Application-Authorization', accessToken || ''),
    })

    return next.handle(req).toPromise()
  }
}

export const httpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AppInterceptor,
    multi: true,
  },
]
