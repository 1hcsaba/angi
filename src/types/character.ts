export interface Character {
  description: string
  id: string
  name: string
  power: string
  side: string
  selected: boolean
  dark: boolean
  won: boolean
  health: number
}

export type CharacterResponse = { characters: Partial<Character>[] }
