type JWT = string

export interface User {
  email: string
  firstName: string
  lastName: string
}

export interface UserCredential {
  token: JWT
  refreshToken: JWT
  user: User
}
